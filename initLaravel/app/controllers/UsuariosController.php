<?php
/**
 * Created by PhpStorm.
 * User: AleM
 * Date: 13/04/15
 * Time: 21:18
 */


//This is the controller that use the entity class and return a view with the correct data.
class UsuariosController extends BaseController {

    /**
     * Mustra la lista con todos los usuarios
     */
    public function getIndex()
    {
        //Con el método all() le estamos pidiendo a la entidad Usuario
        //que busque todos los registros contenidos en esa tabla y los devuelva en un Array.
        $usuarios = Usuario::all();

        // El método make de la clase View indica qué vista vamos a mostrar al usuario
        //y también pasa como parámetro los datos que queramos pasar a la vista.

        //El primer parametro del with() indica el nombre del parametro definido en la
        //clase vista.
        //El segundo parametro es la variable definida en esta clase.
        return View::make('lista')->with('usuarios', $usuarios);
    }

    public function postUsuarios() {
        $new_user = new Usuario();
        $new_user->nombre = Input::get('ìnputName');
        $new_user->save();

        return Redirect::to('/usuarios');
    }
}
?>