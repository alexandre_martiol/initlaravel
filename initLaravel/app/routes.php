<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/', function()
{
	$usuarios = Usuario::all();

	return View::make('lista')->with('usuarios', $usuarios);
});*/


//This function is executed when an user enter in /usuarios. Then, the Controller will execute the correct function depending on the type of request.
//Route::controller("usuarios", "UsuariosController");
Route::get("usuarios", "UsuariosController@getIndex");
Route::post("usuarios", "UsuariosController@postUsuarios");
