<?php
/**
 * Created by PhpStorm.
 * User: AleM
 * Date: 13/04/15
 * Time: 21:17
 */

//This is the entity class that use the structure of the DB table.
class Usuario extends Eloquent { //Todos los modelos deben extender la clase Eloquent
    protected $table = 'usuarios';
}
